# Vipkwd-OAuth-SDK

The SDK for VK-OAuth system.

## Installing.

Install library via composer:

```Php
composer require vipkwd/oauth-sdk
```

## Usage.

## 一. 初始化SDK
```php
include_once('vendor/autoload.php');
use Vipkwd\SDK\OAuth\OAuth as SDK;
use Vipkwd\SDK\OAuth\Action as SdkAction;
use Vipkwd\SDK\OAuth\Restful as SdkRestful;
$instance = new SDK([
    'client_id'     => 'xxxx',
    'client_secret' => 'xxxx',
    'redirect_uri'  => null
    
    'response_type' => 'code',  // In the SDK, the default is `code`
    'scope'         => 'basic', // In the SDK, the default is `basic`
    'state'         => 'xyz',   // In the SDK, the default is `xyz`
]) :Object;
```

## 二、授权模式

### #1、授权码（authorization code）方式
#### ##1.1、获取登录链接: 
```php
$instance->getLoginUrl('code'): string;
```

#### ##1.2、回调页面捕获code换取token:
#### ###1.2.a、自动拉取token用户信息: Response Array: ["tokenData"=> array, "userInfo" => array, "clientId" => string]
```php
$response = $instance->authorizeCodeType(true): array|void;
```

#### ###1.2.b、自动拉取token用户信息: Response Array: ["tokenData"=> array, "userInfo" => array, "clientId" => string]
```php
$response = $instance->authorizeCodeType(true, function (array $tokenData, array &$userInfo, string $clientId) use ($session) {
	$userInfo['age'] = 18;
	$session->set('oauth', $tokenData);
	$session->set('user', $userInfo);
}): array|void;
```

#### ###1.2.c、不拉取token用户信息: Response Array: ["tokenData"=> array, "userInfo" => [], "clientId" => string]
```php
$response = $instance->authorizeCodeType(false, function (array $tokenData, array $userInfo, string $clientId) use ($session) {
	$session->set('oauth', $tokenData);
	// $userInfo === []
}): array|void;
```
#### ###1.3、响应错误: `is_array($response) === false` 时，可使用  `$instance->except`  捕获错误消息Array


### #2、隐藏式（implicit）
#### ##2.1、获取登录链接: 
```php
$instance->getLoginUrl('token'): string;
```

#### ##2.2、授权成功,在回调地址中，用JS捕获地址锚点
    (注意，令牌的位置是URL锚点（fragment），而不是查询字符串（querystring），这是因为 OAuth2.0 允许跳转网址是 HTTP 协议，因此存在"中间人攻击"的风险，而浏览器跳转时，锚点不会发到服务器，就减少了泄漏令牌的风险。)


### #3、凭证式（client credentials）:适用于没有前端的应用(如:命令行)
    注意：这种方式请求地址会暴露APP_SECRET，且给出的令牌，是针对第三方应用的，而不是针对用户的，即有可能多个用户共享同一个令牌。
#### ##3.1、获取凭证式授权(请求捕获JSON响应)
```php
$instance->getTypesUrl('client'): string;
```

#### ##3.2、或者服务端用
```php
// 响应（同授权码方式 1.2.xxx ）
$instance->clientCredentialsType(bool $autoFetchUserInfo = false, ?\Closure $callback = null) 
```


### #4、密码式（password）
#### ##4.1、获取凭证式授权(请求捕获JSON响应)
```php
$instance->getTypesUrl('password'): string;
```

#### ##4.2、或者服务端用
```php
// 响应（同授权码方式 1.2.xxx ）
$instance->passwordType(string $username, string $password, bool $autoFetchUserInfo = false, ?\Closure $callback = null) 
```

## 三、 CURD ...
```php

SdkRestful::get(string $api, array $data = [], string $access_token):array
SdkRestful::post(string $api, array $data = [], string $access_token):array
SdkRestful::put(string $api, array $data = [], string $access_token):array
SdkRestful::delete(string $api, array $data = [], string $access_token):array
SdkRestful::options(string $api, array $data = [], string $access_token):array
SdkRestful::patch(string $api, array $data = [], string $access_token):array

```

## 四、 Cache && Store ...
```php

// 获取本地缓存用户信息
SdkAction::getUserInfo():array

// 获取并刷新当前用户信息
SdkAction::getUserInfo(true):array

// 获取指定用户信息
SdkAction::getUserInfo(uid):array

// 获取用户ID
SdkAction::getUserId():integer|string|null

// 获取当前token对应的 应用ID
SdkAction::getClientId():string

// 获取请求token
SdkAction::getAccessToken():string|null

// 获取本地缓存用户信息
SdkAction::cacheOAuthUser(null):array

// 获取本地用户信息的指定字段
SdkAction::cacheOAuthUser(userInfokey):bool|mixed

// 本地缓存用户信息
SdkAction::cacheOAuthUser(array):bool

// 清空本地缓存用户
SdkAction::deleteOAuthUser():bool

// 获取本地缓存Token信息
SdkAction::cacheOAuthToken(null):array|null

// 本地缓存Token信息
SdkAction::cacheOAuthToken(array):bool

// 获取本地Token信息的指定字段
SdkAction::cacheOAuthToken(userInfokey):bool|mixed

// 清空本地缓存的Token
SdkAction::deleteOAuthToken():bool

```