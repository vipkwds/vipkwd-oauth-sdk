<?php

declare(strict_types=1);

namespace Vipkwd\SDK\OAuth;

use Vipkwd\SDK\OAuth\OAuth as SDK;

class Restful
{
    public static function __callStatic(string $method, array $args)
    {
        $method = strtolower($method);
        if (in_array($method, ['post', 'delete', 'put', 'get', 'options', 'patch'])) {
            $result = (new SDK([]))->resource(
                (string)($args[2]),
                rtrim(
                    (string)($args[0]),
                    '/'
                ),
                $method,
                (array)($args[1] ?? [])
            );
            if ($result) {
                if (isset($result['error']) && isset($result['error_description'])) {
                    $result = [
                        'message' => $result['error'],
                        'data' => $result['error_description'],
                        'code' => 501
                    ];
                }
                return $result;
            }
        }
        return null;
    }
}
